<?php

namespace BookReview\ApiBundle\Controller;

use BookReview\BookBundle\Entity\Review;
use BookReview\BookBundle\Form\ReviewType;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use BookReview\BookBundle\Entity\Book;
use BookReview\BookBundle\Form\BookType;
use Symfony\Component\HttpFoundation\Response;

class BookController extends FOSRestController
{
    /**
     * This method is used to retrieve all the available books and reviews as json object
     * @ApiDoc(
     *  resource=true,
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         }
     *     },
     *     description="Return all available books and reviews",
     *     authentication=false,
     *     authenticationRoles={"IS_AUTHENTICATED_ANONYMOUSLY"},
     *     statusCodes={200= "Return the available books and reviews", 204 = "Return empty json when no book is available"},
     *     output={"class"= "BookReview\BookBundle\Entity\Book"}
     * )
     */
    public function getBooksAction(){
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository('BookReviewBookBundle:Book')->findAll();
        if ($books)
            return $this->handleView($this->view($books, 200));
        else
            return $this->handleView($this->view(null, 204));
    }

    /**
     * This method is used to retrieve information about a specific book as json object
     * @ApiDoc(
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="Integer",
     *          "requirement"="\d+",
     *          "description"="The ID of the book"
     *      }
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         }
     *     },
     *     description="Return the available information about a specific book",
     *     statusCodes={404= "Invalid book ID", 200 = "Return the available information about that book"},
     *     authentication=false,
     *     authenticationRoles={"IS_AUTHENTICATED_ANONYMOUSLY"},
     *     output={"class"= "BookReview\BookBundle\Entity\Book"}
     * )
     */
    public function getBookAction($id){
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookReviewBookBundle:Book')->find($id);

        if (!$book){
            $view = $this->view(null, 404); // Not Found, May Be Available In Future
        } else {
            $view = $this->view($book, 200);
        }

        return $this->handleView($view);
    }

    /**
     * This method is used to post a new book from json object
     * @ApiDoc(
     *  resource=true,
     *  parameters={
     *      {"name"="title", "dataType"="string", "required"=true, "description"="Book Title"},
     *      {"name"="summary", "dataType"="string", "required"=true, "description"="Information Summary about the book"},
     *      {"name"="image", "dataType"="base64", "required"=true, "description"="Base64 code of the image"},
     *      {"name"="author", "dataType"="string", "required"=true, "description"="Book Author"},
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to post a new book from json object",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={401= "User is not logged in", 400= "Bad request", 201 = "Book added successfully"},
     * )
     */
    public function postBookAction(Request $request){
        $auth_check = $this->get('security.authorization_checker');
        if ( !$auth_check->isGranted('IS_AUTHENTICATED_FULLY'))
            return $this->handleView($this->view(null, 401));
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);

        if ($request->getContentType() != 'json'){
            return $this->handleView($this->view(null,400)); // Bad Request
        }
        $data = json_decode($request->getContent(), true);

        // Extract image extension
        $imageData = explode(',', $data['image']);
        $str = $imageData[0];
        $from = "/";
        $to = ";";
        $sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
        $exten = substr($sub,0,strpos($sub,$to));

        // Check image type
        if (!in_array(strtolower($exten),array('png', 'jpeg', 'jpg'))){
            return $this->handleView($this->view("Invalid Image Type", 400));
        }

        // Generate random name for the image && save it
        $fileName = md5(uniqid()).'.'.$exten;
        file_put_contents($this->getParameter('image_directory').'/'.$fileName, base64_decode($imageData[1], true));

        $mimeTyp = "image/".$exten;
        $file = new UploadedFile($this->getParameter('image_directory').'/'.$fileName,$fileName,$mimeTyp,null,null,true);

        $backedData = array(
            'title'=>$data['title'],
            'summary'=>$data['summary'],
            'author'=>$data['author'],
            'image'=>$file,
        );
        $form->submit($backedData);

        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $book->setTitle($form->get('title')->getData());
            $book->setSummary($form->get('summary')->getData());
            $book->setAuthor($form->get('author')->getData());
            $book->setUser($this->getUser());
            $book->setCreated(new \DateTime());
            $book->setImage($fileName);
            $em->persist($book);
            $em->flush();
            return $this->handleView($this->view(null, 201)->setLocation($this->generateUrl('api_book_get_book', ['id'=>$book->getId()])));
        } else {
            unlink($this->getParameter('image_directory').'/'.$fileName);
            return $this->handleView($this->view($form, 400));
        }
    }

    /**
     * This method is used to Edit a specific book
     * @ApiDoc(
     *
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="Integer",
     *          "requirement"="\d+",
     *          "description"="The ID of the book"
     *      }
     *  },
     *  parameters={
     *      {"name"="title", "dataType"="string", "required"=true, "description"="Book Title"},
     *      {"name"="summary", "dataType"="string", "required"=true, "description"="Information Summary about the book"},
     *      {"name"="image", "dataType"="base64", "required"=true, "description"="Base64 code of the image"},
     *      {"name"="author", "dataType"="string", "required"=true, "description"="Book Author"},
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to Edit a specific book",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={400= "Bad Request", 401 = "User not logged in or this user can't edit this book info", 200= "Editing is finished successfully"},
     * )
     */
    public function putBookAction($id, Request $request){
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookReviewBookBundle:Book')->find($id);

        if ($request->getContentType() != 'json'){
            return $this->handleView($this->view(null,400)); // Bad Request
        }

        if (!$book){
            return $this->handleView($this->view(null,400)); // Bad Request
        }

        $auth_check = $this->get('security.authorization_checker');
        if ( $book->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            return $this->handleView($this->view(null, 401));

        $form = $this->createForm(BookType::class, $book);
        $lastImg = $book->getImage();
        $form->handleRequest($request);


        $data = json_decode($request->getContent(), true);

        // Extract image extension
        $imageData = explode(',', $data['image']);
        $str = $imageData[0];
        $from = "/";
        $to = ";";
        $sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
        $exten = substr($sub,0,strpos($sub,$to));

        // Check image type
        if (!in_array(strtolower($exten),array('png', 'jpeg', 'jpg'))){
            return $this->handleView($this->view("Invalid Image Type", 400));
        }

        // Generate random name for the image and save it
        $fileName = md5(uniqid()).'.'.$exten;
        file_put_contents($this->getParameter('image_directory').'/'.$fileName, base64_decode($imageData[1], true));

        $mimeTyp = "image/".$exten;
        $file = new UploadedFile($this->getParameter('image_directory').'/'.$fileName,$fileName,$mimeTyp,null,null,true);

        $backedData = array(
            'title'=>$data['title'],
            'summary'=>$data['summary'],
            'author'=>$data['author'],
            'image'=>$file,
        );


        $form->submit($backedData);
        if ($form->isValid()){
            unlink($this->getParameter('image_directory').'/'.$lastImg);
            $book->setTitle($form->get('title')->getData());
            $book->setSummary($form->get('summary')->getData());
            $book->setAuthor($form->get('author')->getData());
            $book->setImage($fileName);
            $em->flush();
            return $this->handleView($this->view(null, 200)->setLocation($this->generateUrl('api_book_get_book', ['id'=>$book->getId()])));
        } else {
            unlink($this->getParameter('image_directory').'/'.$fileName);
            return $this->handleView($this->view($form, 400));
        }

    }

    /**
     * This method is used to delete a specific book and reviews
     * @ApiDoc(
     *
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="Integer",
     *          "requirement"="\d+",
     *          "description"="The ID of the book"
     *      }
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to delete a specific book and reviews",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={400= "Invalid Book ID", 401 = "User not logged in or this user can't edit this book info", 200= "Book Deleted"},
     * )
     */
    public function deleteBookAction($id){
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookReviewBookBundle:Book')->find($id);

        if (!$book){
            return $this->handleView($this->view(null, 400)); // Bad Request
        }

        $auth_check = $this->get('security.authorization_checker');
        if ( $book->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            return $this->handleView($this->view(null, 401));


        // Get related Reviews
        $reviews = $book->getReviews();
        foreach ($reviews as $review){
            $rv = $em->getRepository('BookReviewBookBundle:Review')->find($review->getId());
            $em->remove($rv);
            $em->flush();
        }

        //Remove Book Image
        $imgName = $book->getImage();
        unlink($this->getParameter('image_directory').'/'.$imgName);

        //Remove Book Record
        $em->remove($book);
        $em->flush();
        return $this->handleView($this->view(null, 200)); // Bad Request

    }

    /**
     * This method is used to post a new review on specific book
     * @ApiDoc(
     *  resource=true,
     *  parameters={
     *      {"name"="comment", "dataType"="string", "required"=true, "description"="User comment on the book"},
     *      {"name"="stars", "dataType"="integer[0-5]", "required"=true, "description"="rate the book between 0-5 stars"},
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to post a new review on specific book",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={401= "User is not logged in", 400= "Bad request, Invalid request content type", 201 = "Review added successfully", 404="Invalid book ID"},
     * )
     */

    public function postBookReviewsAction($book_id, Request $request){

        $auth_check = $this->get('security.authorization_checker');
        if ( !$auth_check->isGranted('IS_AUTHENTICATED_FULLY'))
            return $this->handleView($this->view(null, 401));

        if ($request->getContentType() != 'json'){
            return $this->handleView($this->view(null,400)); // Bad Request
        }
        $data = json_decode($request->getContent(), true);
        $comment = $data['comment'];
        $stars = $data['stars'];
        if ($stars > 5)
            return $this->handleView($this->view(null, 400));

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($book_id);

        // Check Book ID
        if (!$book)
            return $this->handleView($this->view(null, 404));

        $review = new Review();
        $review->setUser($this->getUser());
        $review->setBook($book);
        $review->setComment($comment);
        $review->setStars($stars);
        $review->setCreated(new \DateTime());

        $em->persist($review);
        $em->flush();
        return $this->handleView($this->view(null, 201));
    }

    /**
     * This method is used to Edit a specific review
     * @ApiDoc(
     *
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="Integer",
     *          "requirement"="\d+",
     *          "description"="The ID of the review"
     *      }
     *  },
     *  parameters={
     *      {"name"="comment", "dataType"="string", "required"=true, "description"="User comment on the book"},
     *      {"name"="stars", "dataType"="integer[0-5]", "required"=true, "description"="rate the book between 0-5 stars"},
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to Edit a specific review",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={400= "Bad Request, Invalid request content type or invalid review ID", 401 = "User not logged in or this user can't edit this review", 200= "Editing is finished successfully"},
     * )
     */
    public function putReviewAction($id, Request $request){
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository(Review::class)->find($id);

        if ($request->getContentType() != 'json'){
            return $this->handleView($this->view(null,400)); // Bad Request
        }

        if (!$review){
            return $this->handleView($this->view(null, 400)); // Bad Request
        }

        $auth_check = $this->get('security.authorization_checker');
        if ( $review->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            return $this->handleView($this->view(null, 401));

        $data = json_decode($request->getContent(), true);
        if ($data['stars'] > 5)
            return $this->handleView($this->view(null, 400));

            $review->setComment($data['comment']);
            $review->setStars($data['stars']);
            $book_id = $review->getBook()->getId();
            $em->flush();
            return $this->handleView($this->view(null, 200)->setLocation($this->generateUrl('api_book_get_book', ['id'=>$book_id])));
    }

    /**
     * This method is used to delete a specific review
     * @ApiDoc(
     *
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="Integer",
     *          "requirement"="\d+",
     *          "description"="The ID of the review"
     *      }
     *  },
     *     headers={
     *         {
     *             "name"="Content-Type",
     *             "description"="application/json",
     *             "required"="true"
     *         },
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + Token",
     *             "required"="true"
     *         }
     *     },
     *     description="This method is used to delete a specific review",
     *     authentication=true,
     *     authenticationRoles={"ROLE_ADMIN", "ROLE_USER"},
     *     statusCodes={400= "Invalid Review ID", 401 = "User not logged in or this user can't delete this review", 200= "Review Deleted"},
     * )
     */

    public function deleteReviewAction($id){
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository(Review::class)->find($id);

        if (!$review){
            return $this->handleView($this->view(null, 400)); // Bad Request
        }

        $auth_check = $this->get('security.authorization_checker');
        if ( $review->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            return $this->handleView($this->view(null, 401));

        //Remove Book Record
        $em->remove($review);
        $em->flush();
        return $this->handleView($this->view(null, 200));
    }
}