<?php

namespace BookReview\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="BookReview\BookBundle\Entity\Review", mappedBy="user")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity="BookReview\BookBundle\Entity\Book", mappedBy="user")
     */
    private $books;

    public function __construct()
    {
        parent::__construct();
        $this->reviews = new ArrayCollection();
        $this->books = new ArrayCollection();
    }

    /**
     * Add review
     *
     * @param \BookReview\BookBundle\Entity\Review $review
     *
     * @return User
     */
    public function addReview(\BookReview\BookBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \BookReview\BookBundle\Entity\Review $review
     */
    public function removeReview(\BookReview\BookBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Add book
     *
     * @param \BookReview\BookBundle\Entity\Book $book
     *
     * @return User
     */
    public function addBook(\BookReview\BookBundle\Entity\Book $book)
    {
        $this->books[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param \BookReview\BookBundle\Entity\Book $book
     */
    public function removeBook(\BookReview\BookBundle\Entity\Book $book)
    {
        $this->books->removeElement($book);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBooks()
    {
        return $this->books;
    }
}
