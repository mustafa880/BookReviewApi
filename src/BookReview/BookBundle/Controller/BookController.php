<?php

namespace BookReview\BookBundle\Controller;

use BookReview\BookBundle\Entity\Review;
use BookReview\BookBundle\Form\ReviewType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BookReview\BookBundle\Entity\Book;
use BookReview\BookBundle\Form\BookType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class BookController extends Controller
{
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository('BookReviewBookBundle:Book')->getLatest(10, 0);
        return $this->render('@BookReviewBook/Book/index.html.twig', [
            'books'=>$books,
        ]);
    }

    public function viewAction(Request $request,$id)
    {
        // get Book details..
        $em = $this->getDoctrine()->getManager();
        $bookData = $em->getRepository('BookReviewBookBundle:Book')->find($id);
        if (!$bookData) {
            throw $this->createNotFoundException(
                'No Book found for id '.$id
            );
        }

        // get Book reviews
        $reviews = $bookData->getReviews();

        //get review rate
        $repo = $this->getDoctrine()->getRepository('BookReviewBookBundle:Review');
        $query = $repo->createQueryBuilder('review')
            ->select('review.stars')
            ->where('review.book=:id')
            ->setParameter('id', $id)
            ->getQuery();
        $starsArray = $query->getArrayResult();
        $starsRate= 0;
        foreach ($starsArray as $star)
            $starsRate=$starsRate + (int)$star['stars'];
        if (count($starsArray)>0){
        $starsRate= $starsRate / count($starsArray);
        }
        // If the form is submitted...
        $form = $this->createFormBuilder()->getForm();
        if($request->isMethod('POST')){
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted()){
                $auth_check = $this->get('security.authorization_checker');
                if (!$auth_check->isGranted('ROLE_USER'))
                    throw $this->createAccessDeniedException('You cannot access this page!');
                $review = new Review();
                $review->setBook($bookData);
                $review->setUser($this->getUser());
                $review->setComment($request->request->get('comment'));
                $review->setStars($request->request->get('stars'));
                $review->setCreated(new \DateTime());
                $em->persist($review);
                $em->flush($review);
                return $this->redirectToRoute('view',['id'=>$id]);
            }
        }

        return $this->render('BookReviewBookBundle:Book:view.html.twig', array(
            'book' => $bookData,
            'reviews' => $reviews,
            'rate' => $starsRate
        ));
    }

    public function createAction(Request $request)
    {
        $newBook = new Book();
        $form = $this->createForm(BookType::class, $newBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $auth_check = $this->get('security.authorization_checker');
            if (!$auth_check->isGranted('ROLE_USER'))
                throw $this->createAccessDeniedException('You cannot access this page!');

            /**
             * @var UploadedFile $imageFile
             */
            $imageFile = $newBook->getImage();
            $fileName = md5(uniqid()).'.'.$imageFile->guessExtension();
            $em = $this->getDoctrine()->getManager();
            $imageFile->move($this->getParameter('image_directory'), $fileName);
            $newBook->setImage($fileName);
            $newBook->setUser($this->getUser());
            $newBook->setCreated(new \DateTime());
            $em->persist($newBook);
            $em->flush();
            return $this->redirectToRoute('view', ['id'=>$newBook->getId()]);
        }


        return $this->render('BookReviewBookBundle:Book:create.html.twig', array(
            'form'=>$form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookReviewBookBundle:Book')->find($id);
        $auth_check = $this->get('security.authorization_checker');
        if ($book->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            throw $this->createAccessDeniedException('You cannot access this page!');
        $lastImg = $book->getImage();
        $form = $this->createForm(BookType::class, $book, [
            'action'=>$request->getUri(),
        ]);

        if (!$book) {
            throw $this->createNotFoundException(
                'No Book found for id '.$id
            );
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /**
             * @var UploadedFile $imageFile
             */
            $imageFile = $form->get('image')->getData();
            $fileName = md5(uniqid()).'.'.$imageFile->guessExtension();
            $imageFile->move($this->getParameter('image_directory'), $fileName);
            unlink($this->getParameter('image_directory').'/'.$lastImg);
            $book->setTitle($form->get('title')->getData());
            $book->setSummary($form->get('summary')->getData());
            $book->setAuthor($form->get('author')->getData());
            $book->setImage($fileName);
            $em->flush();
            return $this->redirectToRoute('view',['id'=>$id]);
        }

        return $this->render('BookReviewBookBundle:Book:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookReviewBookBundle:Book')->find($id);
        if (!$book) {
            throw $this->createNotFoundException(
                'No Book found for id '.$id
            );
        }

        $auth_check = $this->get('security.authorization_checker');
        if ($book->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            throw $this->createAccessDeniedException('You cannot access this page!');

        //Remove Related Reviews..
        $reviews = $book->getReviews();
        foreach ($reviews as $review){
            $rv = $em->getRepository('BookReviewBookBundle:Review')->find($review->getId());
            $em->remove($rv);
            $em->flush();
        }

        //Remove Book Image
        $imgName = $book->getImage();
        unlink($this->getParameter('image_directory').'/'.$imgName);

        //Remove Book Record
        $em->remove($book);
        $em->flush();

        return $this->redirectToRoute('book_review_user_homepage');
    }

    public function searchAction(Request $request){
        if ($request->isMethod('POST')){
            $data = $request->get('keywords');
            $repo = $this->getDoctrine()
                ->getRepository('BookReviewBookBundle:Book');
            $query = $repo->createQueryBuilder('book')
                ->where('book.title LIKE :title')
                ->setParameter('title', '%'.$data.'%')
                ->getQuery();
            $results = $query->getResult();

            // Integrate Google Book API, Send List Request
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', 'https://www.googleapis.com/books/v1/volumes?q='.$data.'&maxResults=12');
            $bodyData = $res->getBody();
            $decoded_data = \GuzzleHttp\json_decode($bodyData, true);
            if (array_key_exists('items', $decoded_data))
                $items = $decoded_data['items'];
            else
                $items=array();

            $gResults = new ArrayCollection();
            if (count($items) > 0){
            foreach ($items as $item) {
                $itemID = $item['id'];
                $VolInfo = $item['volumeInfo'];
                $gResult = new ArrayCollection();
                $gResult->set('id', $itemID);
                if (array_key_exists('title', $VolInfo)) {
                    $itemTitle = $VolInfo['title'];
                    $gResult->set('title', $itemTitle);
                }

                if (array_key_exists('imageLinks', $VolInfo)) {
                    $itemImage = $VolInfo['imageLinks']; // Array
                    if (array_key_exists('thumbnail', $itemImage)) $gResult->set('imageLink',$itemImage['thumbnail']);
                    else if (array_key_exists('smallThumbnail', $itemImage)) $gResult->set('imageLink',$itemImage['smallThumbnail']);
                    else if (array_key_exists('small', $itemImage)) $gResult->set('imageLink',$itemImage['small']);

                } else {
                    $gResult->set('imageLink','/uploads/images/default.jpg');
                }
                $gResults->add($gResult);
            }
            }
//            $content = file_get_contents("https://books.google.com/books/content?id=HRTamnkJW94C&printsec=frontcover&img=1&zoom=3&source=gbs_api");
//            $fp = fopen($this->container->getParameter('kernel.root_dir').'/../web/'."imageNew.jpg", "w");
//            fwrite($fp, $content);
//            fclose($fp);

        }
        return $this->render('@BookReviewBook/Book/search.html.twig', ['results'=>$results, 'gresults'=> $gResults->toArray()]);
    }

    public function editReviewAction($id, Request $request){

        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository(Review::class)->find($id);
        if (!$review) {
            throw $this->createNotFoundException(
                'No Review found for id '.$id
            );
        }
        $auth_check = $this->get('security.authorization_checker');
        if ($review->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            throw $this->createAccessDeniedException('You cannot access this page!');
        $form = $this->createForm(ReviewType::class, $review, [
            'action'=>$request->getUri(),
        ]);

        $bookId = $review->getBook()->getId();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $review->setComment($form->get('comment')->getData());
            $review->setStars($form->get('stars')->getData());
            $em->flush();
            return $this->redirectToRoute('view',['id'=>$bookId]);
        }

        return $this->render('@BookReviewBook/Book/editReview.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function deleteReviewAction($id){
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository(Review::class)->find($id);
        if (!$review) {
            throw $this->createNotFoundException(
                'No Review found for id '.$id
            );
        }

        $auth_check = $this->get('security.authorization_checker');
        if ($review->getUser() != $this->getUser() && !$auth_check->isGranted('ROLE_ADMIN'))
            throw $this->createAccessDeniedException('You cannot access this page!');
        $bookId = $review->getBook();
        $em->remove($review);
        $em->flush();
        return $this->redirectToRoute('view',['id'=>$bookId->getId()]);
    }

    public function integrateAction($id)
    {
        $auth_check = $this->get('security.authorization_checker');
        if (!$auth_check->isGranted('ROLE_USER'))
            throw $this->createAccessDeniedException('You cannot access this page!');
        // Send Get Request
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://www.googleapis.com/books/v1/volumes/'.$id);
        $bodyData = $res->getBody();
        $decoded_data = \GuzzleHttp\json_decode($bodyData, true);

        $VolInfo = $decoded_data['volumeInfo'];
        $em = $this->getDoctrine()->getManager();
        $book = new Book();

        if (array_key_exists('title', $VolInfo)) {
            $itemTitle = $VolInfo['title'];
        }

        if (array_key_exists('subtitle', $VolInfo)) {
            $itemSubtitle = $VolInfo['subtitle'];
            $itemTitle =$itemTitle.': '.$itemSubtitle;
        }
        $book->setTitle($itemTitle);

        if (array_key_exists('description', $VolInfo)){
            $itemDescritpion = $VolInfo['description'];
            $book->setSummary($itemDescritpion);
        } else {
            $book->setSummary($itemTitle);
        }

        if (array_key_exists('authors', $VolInfo)) {
            $itemAuthor = $VolInfo['authors']; // Array
            $auth = '';
            foreach (array_reverse($itemAuthor) as $author)
                $auth=$author.', '.$auth;
            $book->setAuthor($auth);
        }

        if (array_key_exists('imageLinks', $VolInfo)) {
            $itemImage = $VolInfo['imageLinks']; // Array
            if (array_key_exists('smallThumbnail', $itemImage)) $thumbnail = $itemImage['smallThumbnail'];
            else if (array_key_exists('thumbnail', $itemImage)) $thumbnail = $itemImage['thumbnail'];
            else if (array_key_exists('small', $itemImage)) $thumbnail = $itemImage['small'];

            // Download Image
            $fileName = md5(uniqid()).'.jpg';
            $content = file_get_contents($thumbnail);
            $fp = fopen($this->container->getParameter('kernel.root_dir').'/../web/uploads/images/'.$fileName, "w");
            fwrite($fp, $content);
            fclose($fp);
            $book->setImage($fileName);
        }
        $book->setUser($this->getUser());
        $book->setCreated(new \DateTime());
        $em->persist($book);
        $em->flush();
        return $this->redirectToRoute('view', ['id'=>$book->getId()]);
    }
}
